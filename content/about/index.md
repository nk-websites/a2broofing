---
title: About A2B Roofing
description:
---

## About A2B Roofing

Here at A2B Roofing, we pride ourselves on delivering cost effective flat roofing solutions at
affordable prices in a quick and timely manner.

We are a fully licensed supply and installation company of various single ply membranes, liquid plastics, felts and mastic asphalt. We also hold Public Liability Insurance set at £5 million and Employers’ Liability Insurance at £10 million, which means that even if the worse were to occur, your property would be fully protected from any damage caused. We are members of various different Trade and Roofing organisations, as well as many Health and Safety regulators, who regularly inspect our works and policies to ensure that we are offering the best and safest roofing solutions. 

We are registered at Companies House England and Wales and are also VAT registered. All of our staff are fully trained in roofing installations and repairs, holding the relevant training cards and certificates in all aspects of roofing and health and safety training. All relevant documentation is available on request.

Based in Cambridge, but covering all of East Anglia, we offer friendly impartial advice, with all quotations offered free of charge. To help keep costs down, we can also support with budgeting and cost-effective solutions.