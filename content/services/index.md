---
title: Roofing Services from A2B Roofing
description:
---

## Roofing Services

Here at A2B Roofing, we offer a range of services to cover all your flat roofing requirements.

We can recover, remove, replace, repair or simply give your roof a deep clean and thorough inspection.

We can bring in independent leak testers, with the ability to find the most concealed penetrations that are often overlooked by the untrained eye.

With minimal disruption, we can reline flintlock gutters or remove and replace them with products that are more aesthetically pleasing.

We offer a fantastic selection of roof-lights, whether polycarbonate or self-cleaning blue glass, as well as balustrade systems, slabbing for balconies, man-safe systems and roof hatches.

A complete list of our services are detailed below, however if you require anything not currently listed, please do contact us as chances are we can still help.

### List of services:
* Flat roof repairs
* Flat roof installations
* Flat roof surveys
* Asbestos roof replacements
* Garage roofing
* Roof-lights, access hatches and handrails/latchways
* Fascia, gutters and soffits (aluminium or uPVC)
* Gutters, hoppers and downpipes (aluminium or uPVC)
* Thermal insulation upgrades and reports on new/old flat roofs
* Electronic and wet testing leak detection
* Skips and Scaffolding