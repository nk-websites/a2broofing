#### "We were very pleased with the replacement of our worn out garage roofing felt with modern long lasting material. The preparation and work done on the day were of a high standard, with great care taken to minimise disruption and clean up afterwards. We have already recommended them to others."

**Gina and Paul Langley**<br />
Satisfied customers