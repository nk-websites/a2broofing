## Flat roofing specialists

Here at A2B Roofing, we pride ourselves on delivering cost effective flat roofing solutions at affordable prices in a quick and timely manner.

With over 30 years’ experience in the field of roofing, there’s nothing our fully trained and
professional roofers don’t understand about all the aspects of your roof. Whether your roof needs a simple repair or a full replacement, we have the workmanship, the knowledge and the experience to meet all of your roofing needs. We handle everything from scaffolds to skips, site security, welfare and everything in between, ensuring minimal disruption to your daily routine.

At A2B Roofing, we strive to give our customers 100% satisfaction, as well as peace of mind by using the best products, that have all been independently tested and certificated to last well over 30 years. In addition to this, all of our flat roofs come complete with a 15-year insurance backed guarantee and a BBA lifespan certificate of 35 years.

We never request payment or deposits up front, all costs are requested on final completion of the work and with your property left clean and tidy.

So, if your roof is in need of repair, replacement or perhaps just a bit of TLC, give A2B Roofing a call today for a no obligation quotation.