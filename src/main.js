// This is the main.js file. Import global CSS and scripts here.
// The Client API can be used here. Learn more: gridsome.org/docs/client-api

import DefaultLayout from '~/layouts/Default.vue';
import ContentLayout from '~/layouts/content.vue';
import '~/assets/scss/main.scss';

export default function(Vue, { router, head, isClient }) {
  // router.scrollBehavior = (to, from, savedPosition) => {
  //   if (to.hash) {
  //     return window.scrollTo({
  //       top: document.querySelector(to.hash).offsetTop,
  //       behavior: 'smooth'
  //     });
  //   } else {
  //     return { x: 0, y: 0 };
  //   }
  // };

  // Set default layout as a global component
  Vue.component('Layout', DefaultLayout);
  Vue.component('ContentLayout', ContentLayout);
}
