// This is where project configuration and plugin options are located.
// Learn more: https://gridsome.org/docs/config

// Changes here require a server restart.
// To restart press CTRL + C in terminal and run `gridsome develop`

const path = require('path');

function addStyleResource(rule) {
  rule
    .use('style-resource')
    .loader('style-resources-loader')
    .options({
      patterns: [path.resolve(__dirname, './src/assets/scss/_*.scss')]
    });
}

module.exports = {
  chainWebpack(config) {
    // Load variables for all vue-files
    const types = ['vue-modules', 'vue', 'normal-modules', 'normal'];

    // or if you use scss
    types.forEach(type => {
      addStyleResource(config.module.rule('scss').oneOf(type));
    });

    // config.mode('development');
  },
  siteName: 'A2B Roofing',
  siteUrl: 'https://www.a2broofing.co.uk/',
  titleTemplate: '%s',
  plugins: [
    // {
    //   use: '@gridsome/plugin-google-analytics',
    //   options: {
    //     id: 'UA-104551751-1'
    //   }
    // },
    // {
    //   use: 'gridsome-plugin-gtm',
    //   options: {
    //     id: 'GTM-M7XZDD7',
    //     enabled: true,
    //     debug: true
    //   }
    // },
    {
      use: '@gridsome/source-filesystem',
      options: {
        path: 'content/**/*.md',
        typeName: 'DocPage'
      }
    },
    {
      use: '@gridsome/source-filesystem',
      options: {
        baseDir: 'content/',
        path: 'blog/**/*.md',
        typeName: 'BlogPage'
      }
    },
    {
      use: '@gridsome/source-filesystem',
      options: {
        path: 'content/**/*.json',
        typeName: 'Items'
      }
    },
    {
      use: '@gridsome/plugin-sitemap',
      options: {
        cacheTime: 600000, // default
        exclude: [],
        config: {
          '/blog/*': {
            changefreq: 'weekly',
            priority: 0.5
          },
          '/about': {
            changefreq: 'monthly',
            priority: 0.7
          }
        }
      }
    }
  ]
};
